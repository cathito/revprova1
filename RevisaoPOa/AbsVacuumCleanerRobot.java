package RevisaoPOa;

public abstract class AbsVacuumCleanerRobot extends AbsBasicRobot implements IVacuumCleaner{
	
	private boolean switchedOn;
//-------------------------------------------------------------------//
	
	public AbsVacuumCleanerRobot(Environment environment) {
		super(environment);
	}
//-------------------------------------------------------------------//
	
	public void turnOn() {
		switchedOn=true;
	}
	public void turnOff() {
		switchedOn=false;
	}
//-------------------------------------------------------------------//
	public boolean isDirty() {
		if(this.environment.getValue(currentPosition)=='S') {       //getValue(currentPosition).equals('S')) {
			return true;
		}else {
			return false;
		}
	}
	
	public void clear(){
		if(this.environment.getValue(currentPosition)=='S') {
			environment.setVelue(currentPosition, ' ');
		}
		//if(isDirty()) {
			//environment.setVelue(currentPosition, ' ');
		//}
	}
//-------------------------------------------------------------------//
	
	public boolean isSwitchedOn() {
		return switchedOn;
	}
//-------------------------------------------------------------------//
	
}
