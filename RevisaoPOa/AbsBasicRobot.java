package RevisaoPOa;


public abstract class AbsBasicRobot implements IRobot { 
	
	protected Environment environment;
	protected Position currentPosition;
	protected Position initialPosition;
	protected Position finalPosition;
	
//-------------------------------------------------------------------//
	public AbsBasicRobot(Environment environment) {
		this.environment=environment; // se tiver q colocar 3x3 e aqui
		initialPosition=new Position(0, 0);
		finalPosition=new Position(2, 2);
		currentPosition=new Position(0, 0);
		
	}
//-------------------------------------------------------------------//
	
	public void setStartPosition(Position position)  throws RobotException{
		try{
			initialPosition=position;
		}catch(java.lang.ArrayIndexOutOfBoundsException e) {
			new RobotException("Posi��o inicial inv�lida.");
		}
		
	}
	
	public void setFinalPosition(Position position) throws RobotException{
		try{
			finalPosition=position;
		}catch(java.lang.ArrayIndexOutOfBoundsException e) {
			new RobotException("Posi��o final inv�lida.");
		}
	}
//-------------------------------------------------------------------//
	
	
	public Environment getEnvironment() { 
		return environment;
	}
	public Position getInitialPosition() {
		return initialPosition;
	}
	public Position getFinalPosition() {
		return finalPosition;
	}
	public Position getCurrentPosition() {
		return currentPosition;
	}
	public void setCurrentPosition(Position currentPosition) {
		this.currentPosition = currentPosition;
	}
//-------------------------------------------------------------------//
	
	public void printPosition() {
		System.out.println( "\nO rob� est� na posi��o "+currentPosition);//.getRow()+", "+currentPosition.getCol()+"." );
	}
//-------------------------------------------------------------------//
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentPosition == null) ? 0 : currentPosition.hashCode());
		result = prime * result + ((environment == null) ? 0 : environment.hashCode());
		result = prime * result + ((finalPosition == null) ? 0 : finalPosition.hashCode());
		result = prime * result + ((initialPosition == null) ? 0 : initialPosition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbsBasicRobot other = (AbsBasicRobot) obj;
		if (currentPosition == null) {
			if (other.currentPosition != null)
				return false;
		} else if (!currentPosition.equals(other.currentPosition))
			return false;
		if (environment == null) {
			if (other.environment != null)
				return false;
		} else if (!environment.equals(other.environment))
			return false;
		if (finalPosition == null) {
			if (other.finalPosition != null)
				return false;
		} else if (!finalPosition.equals(other.finalPosition))
			return false;
		if (initialPosition == null) {
			if (other.initialPosition != null)
				return false;
		} else if (!initialPosition.equals(other.initialPosition))
			return false;
		return true;
	}
	
	
//-------------------------------------------------------------------//
	
	
	

}
