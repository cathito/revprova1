package AtividadeEstrutura;

public class FnFuncao {
    // Variável utilizada pra concatenar o ultimo elemento no print final
    String aux = "0";
    public double fn(int n) throws Exception {
        if (n == 0){
            return 0;
        }
        if (n < 0){
            throw new FnException("Erro! Apenas números maiores que 0!");
        }
        //Chamando método print para printar uma posição a cada chamada recursiva
        print(n);
        //chamada recursiva
        return (fn(n-1) + n/(fatorial(2*n)));
    }
    //Fatorial recursivo
    public double fatorial(int n){
        if(n <= 1){
            return 1;
        }
        return fatorial(n-1) * n;
    }
    //método ultilizado para imprimir posição por posição a cada chamada recursiva
     public void print(int n){
        //Variável auxiliar para concatenar o texto a ser impresso
        String concatenacao = "";
        //Travando ultima opção caso a quantidade de elementos for maior que 5 concatenar com "..."
        if (aux.equals("0"))
            aux = n +"/" + 2*n+"!";
        //teste para verificar a quantidade de elementos para adcionar "..." ou não
        if (n == 6)
            concatenacao = aux + " + ... + ";
        //Teste para printar caso o número seja 5
        if (aux.equals("5/10!"))
            concatenacao += aux + " + ";
        //teste para printar os elementos sem "..." enquanto tiver 5 ou menos elementos
        if(n < 5){
//<<<<<<< HEAD
            concatenacao = n + "/" + 2 * n + "!";
//=======
                concatenacao = n + "/" + 2 * n + "!";
//>>>>>>> 3e233c5541ac261f554e5bc153b5f6dca72cba4f
            //teste para adcioanr o "+", enquanto for diferente de um irá adcionar a adição
            if (n != 1)
                concatenacao += " + ";
        }
        //teste para adciona "=" para concatenar o resultado
        if (n == 1)
            concatenacao += " = ";
        //Printando a cada chamada recursiva o elemento concatenado
        System.out.print(concatenacao);
    }
    public void printDinamico(int n){
        String concatenacao = "";
//<<<<<<< HEAD
        concatenacao = n +"/" + 2*n+"!";
        if (n != 1)
            concatenacao += " + ";
//=======
            concatenacao = n +"/" + 2*n+"!";
            if (n != 1)
                concatenacao += " + ";
//>>>>>>> 3e233c5541ac261f554e5bc153b5f6dca72cba4f
        if (n == 1)
            concatenacao += " = ";
        System.out.print(concatenacao);
    }
}
