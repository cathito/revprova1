package MMC;

import java.util.ArrayList;

public class MMC {
    private int cont = 1;
    private int aux = 2;
    private boolean parada;
    private boolean testeSoma;
    public int calcular(int[] array){
        testeSoma = false;
        parada = true;
        for (int j = 0; j < array.length; j++) {
            if (array[j] != 1)
                parada = false;
            if (array[j]%aux == 0){
                array[j] = array[j]/aux;
                testeSoma = true;
            }
        }
        if (parada == true)
            return cont;
        if (testeSoma)
            cont = cont * aux;
        if (testeSoma == false)
            aux++;
        return calcular(array);
    }
}
