package NovoValorEmVetor;

public class MoverTeste {

	public static void main(String[] args) {
		System.out.println("\n              Trabalhando Com Vetores!");
		
		int [] vetor=new int [10];
		vetor[0]=2;
		vetor[1]=3;
		vetor[2]=10;
		vetor[3]=0;
		vetor[4]=11;
		
		MoverValores mover= new MoverValores();
		System.out.print("\n  Vetor Original: ");
		mover.print(vetor);
		System.out.println("\n===================================================");
//-----------------------------------------------------------------------------------//
		
		System.out.println("\n  Adicionar Valor Na Primeira Posicao");
		mover.PrimeiraPosicao(vetor, 1);
		mover.print(vetor);
		System.out.println("\n------------------------------------------");
//-----------------------------------------------------------------------------------//	
		
		System.out.println("\n  Adicionar Valor Em Quaisquer Posicao:");
		
		System.out.println("\n  Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n\n (A) Posicao Inesistente");
		mover.Meio(vetor, 10, 10);
		
		System.out.println("\n (B)Posicao Posicao Preenchida");
		mover.Meio(vetor, 3, 5);
		mover.print(vetor);
		System.out.println();
		mover.Meio(vetor, 4, 6);
		mover.print(vetor);
		
		System.out.println("\n------------------------------------------");
//-----------------------------------------------------------------------------------//
		
		System.out.println("\n  Adicionar Valor Na Ultima Posicao");
		System.out.println("\n Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n\n Ultima Posicao Livre ");
		mover.UltimaPosicao(vetor, 999);
		mover.print(vetor);
		
		System.out.println("\n\n  Ultima Posicao Existente\n Em Andamento!");
		mover.print(vetor);
		
		
//-----------------------------------------------------------------------------------//
		
		System.out.println("============================================\n");
		
		System.out.println("  Remover Determinado valor");
		
		System.out.println("\n Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n\n  Remover Valor da Primeira Posicao");
		mover.removerPrimeirao(vetor);
		mover.print(vetor);
		System.out.println("\n------------------------------------------");
	//-------------------------------------------------------------------------//
		System.out.println("\n Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n\n  Remover Valor Em Quaisquer Posicao");
		mover.removerMeio(vetor, 2);
		mover.print(vetor);
		
		System.out.println("\n------------------------------------------");
		
		
		
		
		
		
		
	}

}
