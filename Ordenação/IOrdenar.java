package Ordena��o;
/*IOredenar<t>, msms metodos
<<enum>>TipoOrdenacao{ASC,DESC}
<<class>>: IOrdenacao:: -Seleção, - Bolha, - Merge, - Quikshort!!
* */
public interface IOrdenar {
    int[] ordenar(int ordem[]);
    int[] ordenar(int ordem[], TipoOrdenacao tipo);
}