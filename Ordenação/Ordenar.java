package Ordena��o;

public class Ordenar implements IOrdenar {
    int aux;

    public int[] ordenar(int ordem[]) {
        for (int i = 0; i < ordem.length; i++) {
            for (int j = 0; j < ordem.length; j++) {
                if (ordem[i] < ordem[j]) {
                    aux = ordem[i];
                    ordem[i] = ordem[j];
                    ordem[j] = aux;
                }
            }
        }
        return ordem;
    }

    @Override
    public int[] ordenar(int[] ordem, TipoOrdenacao tipo) {
        if (tipo == TipoOrdenacao.ASC) {
            for (int i = 0; i < ordem.length; i++) {
                for (int j = 0; j < ordem.length; j++) {
                    if (ordem[i] < ordem[j]) {
                        aux = ordem[i];
                        ordem[i] = ordem[j];
                        ordem[j] = aux;
                    }
                }
            }
        }
        if (tipo == TipoOrdenacao.DESC) {
            for (int i = 0; i < ordem.length; i++) {
                for (int j = 0; j < ordem.length; j++) {
                    if (ordem[i] > ordem[j]) {
                        aux = ordem[i];
                        ordem[i] = ordem[j];
                        ordem[j] = aux;
                    }
                }
            }
        }
        return ordem;
    }
}



