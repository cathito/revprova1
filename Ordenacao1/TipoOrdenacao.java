package Ordenacao1;

import java.util.Objects;

public class TipoOrdenacao {
    private String tipo;

    public TipoOrdenacao(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
}
