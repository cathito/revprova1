package Ordenacao1;

public class Ordenar implements IOrdenar {
        /*
        -Preencher com números aleatórios
        Random n = new Random();
        for (int i = 0; i < this.vetor.length ; i++) {
            vetor[i] = n.nextInt(100);
        }*/

    public void status(int dados[]){
        for (int i = 0; i < dados.length; i++) {
            System.out.println(dados[i]);
        }
    }

    public void ordenar(int dados[]){
        int aux;
        for (int i = 0; i < dados.length; i++) {
            for (int j = 0; j < dados.length; j++) {
                if (dados[j] > dados[i]) {
                    aux=dados[j];
                    dados[j] = dados[i];
                    dados[i] = aux;
                }
            }
        }
    }

    @Override
    public void ordenar2(int[] dados, TipoOrdenacao tipo) {
        int aux;
        if (tipo.equals("ASC")) {
            for (int i = 0; i < dados.length; i++) {
                for (int j = 0; j < dados.length; j++) {
                    if (dados[j] > dados[i]) {
                        aux=dados[j];
                        dados[j] = dados[i];
                        dados[i] = aux;
                    }
                }
            }
        }
        if (tipo.equals("DESC")) {
            for (int i = 0; i < dados.length; i++) {
                for (int j = 0; j < dados.length; j++) {
                    if (dados[j] < dados[i]) {
                        aux=dados[j];
                        dados[j] = dados[i];
                        dados[i] = aux;
                    }
                }
            }
        }
    }
}
