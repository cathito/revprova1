package Aula2_3;

import java.util.Random;
import java.util.Scanner;

public class OrdenandoTeste {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Random random = new Random();
		
		Ordenando or = new Ordenando();
		
		int Idade[]=new int[10];
		
		
		System.out.println("Original!");
		for (int i = 0; i < Idade.length; i++) {
			Idade[i]=random.nextInt(100);
			System.out.print(Idade[i]+", ");
		}
		System.out.println("\n\nOrdenado Crescente!");
		or.Ordenar(Idade);
		
		System.out.println("\n\nOrdenado Decrescente!");
		or.Ordenar(Idade, "Decr");

	}

}
