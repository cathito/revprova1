package Aula2_3;

public class Ordenando {
	
//--------------------------------------------------------------------//
	
	public void Ordenar(int []Idade) {
		for (int i = 0; i < Idade.length-1; i++) {
			int valorInicial=Idade[i];  //salvando Valor inicial
			int prox=i;  // salvar a nova posicao q o valor inicial vai ser colocado
			int menor=Idade[i]; // salvando o menor valor
			
			for (int j = i+1; j < Idade.length; j++) {
				if(Idade[j]<menor) { 
					menor=Idade[j];
					prox=j;
				}
			}
			Idade[i]=menor;
			Idade[prox]=valorInicial;
		}
		
		for (int i = 0; i < Idade.length; i++) {
			System.out.print(Idade[i]+", ");
		}
		
	}
//--------------------------------------------------------------------//
	public void Ordenar(int []Idade,String TipoOrdenacao) { //sem aplicar modelo enum
		
		if( TipoOrdenacao.equals("ASC") || TipoOrdenacao.equals("asc") ) {
			Ordenar(Idade);
		}else {
			for (int i = 0; i < Idade.length-1; i++) {
				int valorInicial=Idade[i];
				int prox=i;
				int Maior=Idade[i];
				
				for (int j = i+1; j < Idade.length; j++) {
					if(Idade[j]>Maior) {
						Maior=Idade[j];
						prox=j;
					}
				}
				Idade[i]=Maior;
				Idade[prox]=valorInicial;
			}
			
			for (int i = 0; i < Idade.length; i++) {
				System.out.print(Idade[i]+", ");
			}
			
		}
		
	}
//--------------------------------------------------------------------//
	
	
	
	
	

}
