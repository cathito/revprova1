package Aula1;

public class Somar1_10 {
	
//===================== fORMA TRADICIONAL =====================//
	public void ContarTradicional(int v) {
		for (int i = 0; i < v; i++) {
			System.out.print(i+1 +", ");
		}
	}
	
	public void SomarTradicional(int v) {
		int soma=0;
		int VlPrimario=1;
		for (int i = 0; i < v; i++) {
			soma+=VlPrimario;
			VlPrimario++;
			System.out.print(soma+", ");
		}

	}
	public void ValorFinal(int v) {
		int soma=0;
		int VlPrimario=1;
		for (int i = 0; i < v; i++) {
			soma+=VlPrimario;
			VlPrimario++;
		}
		System.out.println(soma);

	}
//----------------------------------------------//
	
	
//===================== FORMA ESTRUTURADA =====================//
	
	public int ContarEstruturado(int r) {
		
		if (r < 2) {
            return r;
        } else {
            return ContarEstruturado(r-1);
        }
		
	}
	public int SomarEstruturado(int r) {
		if (r < 2) {
            return r;
        } else {
        	
            return SomarEstruturado(r - 1) + SomarEstruturado(r - 2);
        }
	}
	
	
	

}
