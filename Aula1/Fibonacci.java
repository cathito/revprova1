package Aula1;

import java.util.Scanner;

public class Fibonacci {
	
//===================== fORMA TRADICIONAL =====================//
	
	public int SequenciaDeTermos(int v) {
		int soma=0;
		int Primario = 0;
		int Secundario = 0;
		
		for (int i = 0; i < v; i++) {
			if (i==1) {
				Secundario = 1;
			}
			soma=Primario+Secundario;
			Primario=Secundario;
			Secundario=soma;
			System.out.print(soma+", ");
		}
		return soma;
	}
//----------------------------------------------//
	public int UltimoTermo(int v) {
		int soma=0;
		int Primario = 0;
		int Secundario = 0;
		
		for (int i = 0; i < v; i++) {
			if (i==1) {
				Secundario = 1;
			}
			soma=Primario+Secundario;
			Primario=Secundario;
			Secundario=soma;
		}
		return soma;
	}
//----------------------------------------------//
	
	
//===================== FORMA ESTRUTURADA =====================//
	public int UltimoTermoEstruturado(int n) {
		if (n < 2) {
            return n;
        } else {
            return UltimoTermoEstruturado(n - 1) + UltimoTermoEstruturado(n - 2); 
        }
	}
	
//=============================================================//
	/*public int SequenciaDeTermosEstruturado(int v) {
		
		int soma=0;
		int Primario = 0;
		int Secundario = 0;
		
		if (v<2) {
			return soma;
		}else {
			soma=Primario+Secundario;
			Primario=Secundario;
			Secundario=soma;
			v--;
			sout(soma);
		}
		return v;
	}
	private void sout(int soma) {
		System.out.println(soma+", ");

	}*/
//=============================================================//
	
	

}
