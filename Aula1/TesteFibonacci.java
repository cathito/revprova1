package Aula1;

import java.util.Scanner;

public class TesteFibonacci {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		Fibonacci f=new Fibonacci();
		
		System.out.println("\n//----------------- FIBONACCI TRADICIONAL ------------------\\\\\n");
		
		System.out.print("\n     Termos Desejado Para a Sequencia De Fibonacci: ");
		int Termo=input.nextInt();
		
		System.out.println("\n     O Ultimo Numero da Sequencia de Fibonacci �: "+f.UltimoTermo(Termo));
		
		System.out.print("\n     A sequencia de fibonacci �: ");
		f.SequenciaDeTermos(Termo);
		
		System.out.println("\n\n\\\\-----------------------------------------------------------//");
		
//======================================================================================================================//
		
		System.out.println("\n\n//------------------- FIBONACCI ESTRUTUAL -------------------\\\\\n");
		
		System.out.print("\n     Termos Desejado Para a Sequencia De Fibonacci: "+Termo);
		//int Termo=input.nextInt();
		
		System.out.println("\n     O Ultimo Numero da Sequencia de Fibonacci �: "+f.UltimoTermoEstruturado(Termo));
		
		System.out.print("\n     A sequencia de fibonacci �: ");
		//f.SequenciaDeTermosEstruturado(Termo);
		
		System.out.println("\n\n\\\\-----------------------------------------------------------//");
		
//======================================================================================================================//
		
		
	}
}
