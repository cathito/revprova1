package Aula1;

import java.util.Scanner;

public class TesteSoma {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Somar1_10 s = new Somar1_10();
		
		
		System.out.println("\n//-------------------- SOMA TRADICIONAL ---------------------\\\\\n");
		
		System.out.print("\n     Termos Desejado Para Ser Somado: ");
		int Termo=input.nextInt();
		
		System.out.print("\n     Sequencia a Ser Somada: ");
		s.ContarTradicional(Termo);
		
		System.out.print("\n     Sequencia de Soma: ");
		s.SomarTradicional(Termo);
		
		System.out.print("\n     O Ultimo Numero da Soma �: ");
		s.ValorFinal(Termo);
		
		System.out.println("\n\n\\\\-----------------------------------------------------------//\n\n");
		
		System.out.println("     Sequencia a Ser Somada: "+s.ContarEstruturado(10));
		
		System.out.println("     Sequencia de Soma: "+s.ContarEstruturado(10));
		
		System.out.println("     O Ultimo Numero da Soma �: "+s.SomarEstruturado(10));

	}

}
