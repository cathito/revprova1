package Aula1;

import java.util.Scanner;

public class TestFib2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Fibonacci2 f=new Fibonacci2();
		
		System.out.println("\n\n//----------------- FIBONACCI ESTRUTUAL ------------------\\\\\n");
		
		System.out.print("\n     Termos Desejado Para a Sequencia De Fibonacci: ");
		int Termo=input.nextInt();
		
		System.out.println("\n     O Ultimo Numero da Sequencia de Fibonacci �: "+f.fibo(Termo));
		
		System.out.print("\n     A sequencia de fibonacci �: ");
		f.SequenciaDeTermosEstruturado(Termo);
		
		System.out.println("\n\n\\\\-----------------------------------------------------------//");
		System.out.println(f.mult(Termo));

	}

}
