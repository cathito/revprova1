package Fatorial;

public class CalculoException extends Exception {
    public CalculoException(String message) {
        super(message);
    }
}
