package Fatorial;

public class Calcular {
    String aux = "0";

    public double fatorial(int num) {
        if (num <= 1){
            return 1;
        }
        return fatorial(num-1) * num;
    }
    public double f(int num) throws CalculoException{
        if (num < 0) {
            throw new CalculoException("Numero Invalido!");
        }
        if (num == 0) {
            return 0;
        }
        //print(num);
        return f(num - 1) + (num / (fatorial(num*2)));
    }

    public void print(int num) {
        String eq = " ";
        if (aux.equals("0")) {
            aux = num + "/" + 2*num + "!";
        }
        if (num == 5) {
            eq += aux + " ... + ";
        }
        if (num <= 4){
            eq += num +"/"+2*num+"!";
            if (num != 1) {
                eq += " + ";
            }
        }

        if (num == 1){
            eq += " = ";
        }
        System.out.print(eq);
    }
}
