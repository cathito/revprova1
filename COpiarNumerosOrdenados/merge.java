package COpiarNumerosOrdenados;

public class merge {
    public int[] unir(int[]a , int[] b){
        int[]resul = new int[a.length+b.length];
        int inicioa=0, iniciob=0,inicioresul=0;
        while(inicioa<a.length && iniciob<b.length){
            if(a[inicioa]>b[iniciob]){
                resul[inicioresul]=b[iniciob];
                iniciob++;

            }else{
                resul[inicioresul]=a[inicioa];
                inicioa++;
            }
            inicioresul++;
        }
        while(iniciob<b.length){
            resul[inicioresul]=b[iniciob++];
            inicioresul++;
        }

        return resul;
    }
}
