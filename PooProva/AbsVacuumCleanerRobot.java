package PooProva;

public abstract class AbsVacuumCleanerRobot extends AbsBasicRobot implements IVacuumCleaner  {
	
	private boolean switchedOn;
	
	
	public AbsVacuumCleanerRobot(Environment environment){
		super(environment);
	}
	
	
	public void turnOn() {// M�todo para ligar o rob�.
		switchedOn=true;
	}
	public void turnOff() {// M�todo para desligar o rob�.
		switchedOn=false;
	}
	public boolean isDirty() {
		if(this.environment.getValue(currentPosition)=='S') {       //getValue(currentPosition).equals('S')) {
			return true;
		}else {
			return false;
		}
	}
	
	
	public void clean() {
        if (environment.getValue(currentPosition) == 'S')
            this.environment.setValue(currentPosition, ' ');
    }
	
	public boolean isSwitchedOn() {
        return switchedOn;
    }
	

}
