package PooProva;


public interface IRobot {
	
	void setStartPosition(Position position) throws RobotException;
	
	public void setFinalPosition(Position position)throws RobotException;

    public void printPosition();

    public Position getCurrentPosition();

    public Position move();

}
