package PooProva;


public abstract class AbsBasicRobot implements IRobot {
	protected Environment environment;// Atributo que armazena o ambiente do rob�.
	protected Position currentPosition;// Atributo que armazena posi��o atual do rob�.
	protected Position initialPosition;// Atributo que armazena posi��o inicial (origem) do rob�.
	protected Position finalPosition;// Atributo que armazena posi��o final (destino) do rob�.
	
	public AbsBasicRobot(Environment environment) {
		this.environment = environment;
		initialPosition=new Position(0,0);
		finalPosition=new Position(2,2);
		currentPosition=new Position(0,0);
	}
	
	
	public Environment getEnvironment() {
		return environment;
	}public Position getCurrentPosition() {
		return currentPosition;
	}public Position getFinalPosition() {
		return finalPosition;
	}public Position getInitialPosition() {
		return initialPosition;
	}
	
	
	/*public void setStartPosition(Position position)  throws RobotException{
		try{
			initialPosition=position;
		}catch(java.lang.ArrayIndexOutOfBoundsException e) {
			new RobotException("Posi��o inicial inv�lida.");
		}
	}
	
	public void setFinalPosition(Position position) throws RobotException{
		try{
			finalPosition=position;
		}catch(java.lang.ArrayIndexOutOfBoundsException e) {
			new RobotException("Posi��o final inv�lida.");
		}
	}
	*/
	public void setStartPosition(Position position) throws RobotException {
        if ((position.getCol() > this.environment.getWidth()) && (position.getRow() > this.environment.getLength()))
            throw new RobotException("Posi��o Inicial Inv�lida - Fora dos Limites da �rea.");
        else
            initialPosition = position;
    }

    @Override
    public void setFinalPosition(Position position) throws RobotException {
        if ((position.getCol() > this.environment.getLength()) && (position.getRow() > this.environment.getWidth()))
            throw new RobotException("Posi��o Final Inv�lida - Fora dos Limites da �rea.");
        else
            finalPosition = position;
    }
	
	
	public void printPosition() {//M�todo que imprime a posi��o do rob� no formato "\nO rob� est� na posi��o"+<posi��o>+"."
		System.out.println("\nO rob� est� na posi��o "+currentPosition+".");
	}

	
	

}
