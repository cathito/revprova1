package PooProva;

public class Environment {
	private char[][] environment;
	int width;
	int length;
	
	public Environment(char[][] environment) {
		this.environment = environment;
	}
	
	public Environment(int width, int length) {
		this.width = width;
        this.length = length;
        this.environment = new char[2][2];
	}
	
	public char getValue(Position position) {
		return this.environment[position.getRow()][position.getCol()];// Retorna o valor do ambiente na posi��o informada.
	}
	public int getWidth() {// Retorna a largura do ambiente.
		width = 2;
		return width;
	}
	public int getLength() {// Retorna o comprimento do ambiente.
		length = 2;
		return length;
	}
	public void setValue(Position position,char value) {//M�todo que define o valor na posi��o informada do ambiente.
		this.environment[position.getRow()][position.getCol()] = value;
	}
			
	public String  printEnvironment() {
		System.out.println("::: AMBIENTE :::");

        for (int i = 0; i <= 2;  i++){ //getLength();

            System.out.print("[");

            for (int j = 0; j <= 2;  j++)//getWidth()
                System.out.print(environment[j][i] + ", ");

            System.out.print("]\n");
        }
        return null;
		
	}
	protected String  printEnvironmentt() {

        for (int i = 0; i <= 2;  i++){ //getLength();

            System.out.print("[");

            for (int j = 0; j <= 2;  j++)//getWidth()
                System.out.print(environment[j][i] + ", ");

            System.out.print("]\n");
        }
        return null;
		
	}
	
	public String toString() {
        return printEnvironmentt();
    }
	
	
	
	
}
