package PooProva;

public class Position {
	private int row;// Atributo que armazena o �ndice da linha.
	private int col;// Atributo que armazena o �ndice da coluna
	
	public Position(int row, int col) {
		this.row=row;
		this.col=col;
	}
	
	public int getRow() {//Retorna a linha da posi��o.
		return row;
	}public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {//Retorna a coluna da posi��o.
		return col;
	}public void setCol(int col) {
		this.col = col;
	}
	
	
	public boolean equals(Object obj) {
		/*if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (col != other.col||row != other.row)
			return false;
		return true;*/
		if (row == col)
            return true;
        else
            return false;
	}
	
	
	public String toString() {// M�todo que retorna uma string no formato "["+ <linha> +", "+ <coluna> +"]".
		return "["+ row +", "+ col +"]";
	}

	
	
}
