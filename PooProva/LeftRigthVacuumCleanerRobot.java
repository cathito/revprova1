package PooProva;

public class LeftRigthVacuumCleanerRobot extends AbsVacuumCleanerRobot{

	public LeftRigthVacuumCleanerRobot(Environment environment) {
		super(environment);
	}
	
	public Position move() {
        if (currentPosition.getRow() < 2){
            currentPosition.setRow(currentPosition.getRow() + 1);
            return currentPosition;
        }else {
            currentPosition.setCol(getCurrentPosition().getCol() + 1);
            currentPosition.setRow(0);
            return currentPosition;
        }
    }

	@Override
	public void clear() {
		super.clean();
		
	}

	
	
	
	

}
