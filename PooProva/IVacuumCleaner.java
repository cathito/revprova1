package PooProva;

public interface IVacuumCleaner {
	void turnOn();
	void turnOff();
	boolean isDirty();
	void clear();
	
}
