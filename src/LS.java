
package src;

import javax.swing.*;

public class LS implements TL{
    No inicio, fim;
    int totalNos;

    public LS() {
        inicio = fim = null;
        totalNos = 0;
    }

    public int getTotalNos() {
        return totalNos;
    }

    public boolean estaVazia(){
        if (getTotalNos() == 0 ){
            return true;
        }
        return false;
    }
    public void adicionarInicio(No n) {
        if (estaVazia()) {
            inicio = fim = n;
        } else {
            n.prox = inicio;
            inicio = n;
        }
        totalNos++;
    }

    public void adicionarFim(No n){
        if(estaVazia()){
            inicio = fim = n;
        }else{
            fim.prox = n;
            fim = n;
        }
        totalNos++;
    }

    public void remover(No n){
        No noAtual;
        No noAnterior;
        noAtual = noAnterior = inicio;
        int contador = 1;

        if (estaVazia() == false){
            while (contador <= getTotalNos() && noAtual.dado != n.dado){
                noAnterior = noAtual;
                noAtual = noAtual.prox;
                contador++;
            }

            if(noAtual.dado == n.dado){
                if ( getTotalNos() == 1){
                    inicio = fim = null;
                }
                else{
                    if (noAtual == inicio){
                        inicio = noAtual.prox;
                    }
                    else{
                        noAnterior.prox = noAtual.prox;
                    }
                }
                totalNos--;
            }
        }
    }
    public void listar(){
        No temp = inicio;
        String valores = "";
        int contador = 1;
        if ( estaVazia() == false ){
            while (contador <= getTotalNos()){
                valores += Integer.toString((Integer) temp.dado)+"-";
                temp = temp.prox;
                contador++;
            }
        }
        System.out.println(valores);
       
    }
}
