package aaaa;

import java.util.Arrays;

public class QuickSort {
	 public static void main(String[] args) {
         int[] a = {1, 4, 2,-1};
         quick(a, 0, a.length-1 );
         System.out.println(Arrays.toString(a));
     }
     public static void quick(int[] a, int esq, int dir) {
         if (esq < dir) {
             int j = espalhar(a, esq, dir);
             quick(a, esq, j-1);
             quick(a, j+1 , dir);
         }
     }
     public static int espalhar(int[] a, int esq, int dir) {
         int i = esq+1;
         int j = dir;
         int pivo = a[esq];
         while (i <=j) {
             //ordenar em ordem crescente
            /* if (a[i] > pivo) i++;
             else if (a[j] < pivo)j--;*/
            //orrdenar em ordem decrescente
             if (a[i] > pivo) i++;
             else if (a[j] < pivo)j--;
             else if (i <= j) {
                 trocar(a, i, j);
                 i++;
                 j--;
             }
         }
         trocar(a,esq,j);
         return j;
     }

     public static void trocar(int[] a, int i, int j) {
         int aux = a[i];
         a[i] = a[j];
         a[j] = aux;
     }
}
