package RevisaoPO;

public abstract class AbsVacuumCleanerRobot extends AbsBasicRobot implements IVacuumCleaner{
	
	private boolean switchedOn;
//-------------------------------------------------------------------//
	
	public AbsVacuumCleanerRobot(Environment environment) {
		super(environment);
	}
//-------------------------------------------------------------------//
	
	public void turnOn() {
		switchedOn=true;
	}
	public void turnOff() {
		switchedOn=false;
	}
//-------------------------------------------------------------------//
	public boolean isDirty() {
		if(environment[super.currentPosition.getRow()][super.currentPosition.getCol()].equals('S')) {
			return true;
		}else {
			return false;
		}
	}
	
	public void clear(){
		environment [currentPosition.getRow()] [super.currentPosition.getCol()].setVelue(super.currentPosition, ' ');
	}
//-------------------------------------------------------------------//
	
	public boolean isSwitchedOn() {
		return switchedOn;
	}
//-------------------------------------------------------------------//
	
}
